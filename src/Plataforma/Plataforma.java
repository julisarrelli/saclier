package Plataforma;





import java.util.Vector;

import BaseDeDatos.LibroDB;
import BaseDeDatos.ReservaDB;
import BaseDeDatos.UsuarioDB;
import Libros.Ejemplar;
import Libros.Libro;
import Libros.Reserva;
import Usuarios.Usuario;

public class Plataforma {
	
    private static Plataforma INSTANCE;



	private Plataforma() {

	}

    public static Plataforma getInstance() {
    	
    	if (INSTANCE==null){
    		INSTANCE = new Plataforma();
    	}
    	
    		return INSTANCE;
    }
    

    public Libro getLibro (Integer ISBN)
    {
    	
    	return LibroDB.getLibro(ISBN);
    	//return libros.get(ISBN);
    }
    

    
    public void eliminarReservas(int idUsuario)
    {
    	
    	ReservaDB.EliminarReservas(idUsuario);
    }
    
    public void eliminarUsuario(int idUsuario)
    {
    	
    	eliminarReservas(idUsuario);
    	UsuarioDB.EliminarUsuario(idUsuario);
    	
    	
    	
    	
    }
    
    public Reserva reservarLibros (Vector<Libro> libros,Usuario usuario)
    {
    	
    	java.util.Date utilDate = new java.util.Date();

    	java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
    	
    	
    	Reserva reserva= new Reserva(usuario, sqlDate);
    	
    	Ejemplar ejemplar;
    	boolean flag=false;
    	
    	for(Libro libro:libros){
    		
    		if(LibroDB.getEjemplarDisponible(libro)!=null)
    		{
    			ejemplar=LibroDB.getEjemplarDisponible(libro);
    			
    			reserva.agregarEjemplar(ejemplar);
    			
    			if(!ReservaDB.verificarExistenciaReserva(ejemplar))flag=true;
    			
    			
    		}
    		else{
    			System.out.println("No hay ejemplares disponibles de: "+libro.getTitulo());
    		}
    		
    		}
    	
    	if(flag)
    	{
    	
    	ReservaDB.insertarReserva(reserva, usuario);
    	}
    	
    	
    	
    	if(reserva.getEjemplares()==null){
    		System.out.println("No se encuentran ejemplares disponibles de ningun libro");
    		return null;
    	}
    	else
    		
    		
    	
    	return reserva;
    }

	public void addUsuario(Usuario usuario) {
		UsuarioDB.insertarUsuario(usuario, usuario.getTipo());
		
	}


	
    
}
