package BaseDeDatos;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

import Libros.Autor;
import Libros.Libro;

public  class AutorDB extends DataBase {


	public static void insertarAutor(Autor autor)
	{
		try
		{

			Connection conexion = Conectar();
			Statement st = conexion.createStatement();
			String nombre=autor.getNombre();
			String apellido=autor.getApellido();
			String DNI=autor.getDNI();

			java.sql.CallableStatement funcion=conexion.prepareCall("{call "
					+ "InsertarAutor('"+nombre+"','"+apellido+"','"+DNI+"')}");


			funcion.execute();


			cerrarConexion(conexion, st);
		}
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, "No se pudo insertar autor");
		}

	}


	public static boolean verificarExistenciaAutor(Autor autor,Libro libro)
	{
		try
		{

			Connection conexion = Conectar();
			Statement st = conexion.createStatement();

			ResultSet resultado=st.executeQuery("Select * FROM Libro_has_Autor  WHERE Autor_idAutor='"+getIdAutor(autor)+"' AND Libro_ISBN='"+libro.getISBN()+"'");
			if(resultado.next())
			{

				return true;
			}

			return false;

		}
		catch (Exception e)
		{

			e.printStackTrace();return false;
		}
	}




	public static int getIdAutor(Autor autor){

		try
		{

			Connection conexion = Conectar();
			Statement st = conexion.createStatement();

			ResultSet resultado=st.executeQuery("Select * FROM Autor WHERE DNI='"+autor.getDNI()+"'");
			resultado.next();

			return resultado.getInt(1);
		}
		catch (Exception e)
		{

			e.printStackTrace();
			return 0;
		}

	}

	/*public static Vector<Autor> getAutores(Libro libro){

		try
		{

			Connection conexion = Conectar();
			Statement st = conexion.createStatement();

			ResultSet resultado=st.executeQuery("Select Autor_idAutor FROM Libro where Libro_ISBN='"+libro.getISBN()+"'");
			resultado.next();

			return resultado.getInt(1);
		}
		catch (Exception e)
		{

			e.printStackTrace();
			return 0;
		}

	}

	 */



}
