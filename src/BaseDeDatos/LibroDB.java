package BaseDeDatos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import Libros.Autor;
import Libros.Ejemplar;
import Libros.Libro;
import Usuarios.Usuario;

public class LibroDB extends DataBase {
	
	
	public static void insertarLibro(Libro libro)
	{
		try
		{
			
			Connection conexion = Conectar();
			
			CallableStatement funcion1=conexion.prepareCall("{call "
					+ "InsertarLibro('"+libro.getISBN()+"','"+libro.getTitulo()+"','"+libro.getCantPags()+"')}");
			
			funcion1.execute();
			

			
			
			

		cerrarConexion(conexion);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	
	public static void insertarAutorAlibro(Libro libro,Autor autor)
	{
		try
		{
			
			Connection conexion = Conectar();
			CallableStatement funcion2=conexion.prepareCall("{call AsignarAutorLibro"
					+ "('"+libro.getISBN()+"','"+AutorDB.getIdAutor(autor)+"','"+autor.getDNI()+"')}");
			funcion2.execute();
			
			cerrarConexion(conexion);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
	
	
	public static Ejemplar getEjemplarDisponible(Libro libro)
	{
		
		try {
			
			Connection conexion=Conectar();
			Statement st = conexion.createStatement();
			
			ResultSet resultado=st.executeQuery("SELECT * FROM Ejemplar WHERE Libro_ISBN='"+libro.getISBN()+"' AND Estado_idEstado=1");
			
			int id;
			int ISBN;
			
			while(resultado.next())
			{
				id=resultado.getInt(1);
				ISBN=resultado.getInt(2);
				Ejemplar ejemplar=new Ejemplar(ISBN);
				ejemplar.setId(id);
				
				return ejemplar;
			}
			
			return null;
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static Libro getLibro(int ISBN1)
	{
		
		try {
			
			Connection conexion=Conectar();
			Statement st = conexion.createStatement();
			
			ResultSet resultado=st.executeQuery("SELECT * FROM Libro WHERE ISBN='"+ISBN1+"'");
			
		
			
			while(resultado.next())
			{
				int ISBN=resultado.getInt(1);
				String titulo=resultado.getString(2);
				int cantPags=resultado.getInt(3);
				Libro libro=new Libro(ISBN, titulo, cantPags);
				
				return libro;
			}
			
			return null;
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
		
	}
	
	public static Ejemplar getEjemplarYaReservado(Usuario usuario,Libro libro)
	{
		try
		{
			Connection conexion = Conectar();					
			Statement st = conexion.createStatement();
			
		ResultSet resultado=st.executeQuery("SELECT * FROM Ejemplar WHERE Libro_ISBN='"+libro.getISBN()+"' AND Estado_idEstado=3 AND Reserva_Usuario_idUsuario='"+UsuarioDB.getIdUsuario(usuario)+"'");
			
			int id;
			int ISBN;
			
			while(resultado.next())
			{
				id=resultado.getInt(1);
				ISBN=resultado.getInt(2);
				Ejemplar ejemplar=new Ejemplar( ISBN);
				ejemplar.setId(id);
				
				return ejemplar;
			}
			
			return null;

		

		}
		catch (Exception e)
		{

			e.printStackTrace();
		}
		return null;
	}
	
	
	
	
	
}
